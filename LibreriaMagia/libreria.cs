﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaMagica
{
    public class libreria
    {
        public float promedios(Int16[] thisArray)
        {
            Int16 res = 0;
            for (Int16 count = 0; count < thisArray.Length; count++)
            {
                res += thisArray[count];
            }
            return (res / thisArray.Length);
        }

        public int mostSold(Int16[] thisArray)
        {
            Int16 maxValue = thisArray.Max();
            int pos = Array.IndexOf(thisArray,maxValue);
            
            return pos;
        }

    }
}
