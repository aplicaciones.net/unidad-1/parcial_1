﻿namespace WindowsFormsApplication1
{
    partial class UNO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_Veces = new System.Windows.Forms.TextBox();
            this.textBox_Clave = new System.Windows.Forms.TextBox();
            this.textBox_Nombre = new System.Windows.Forms.TextBox();
            this.textBox_Dia = new System.Windows.Forms.TextBox();
            this.textBox_Cantidad = new System.Windows.Forms.TextBox();
            this.label_Veces = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button_Next = new System.Windows.Forms.Button();
            this.button_StoreTxt = new System.Windows.Forms.Button();
            this.button_ShowAll = new System.Windows.Forms.Button();
            this.button_Exit = new System.Windows.Forms.Button();
            this.button_Calcular = new System.Windows.Forms.Button();
            this.button_GoDos = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox_Veces
            // 
            this.textBox_Veces.Location = new System.Drawing.Point(55, 13);
            this.textBox_Veces.Name = "textBox_Veces";
            this.textBox_Veces.Size = new System.Drawing.Size(31, 20);
            this.textBox_Veces.TabIndex = 0;
            // 
            // textBox_Clave
            // 
            this.textBox_Clave.Location = new System.Drawing.Point(28, 88);
            this.textBox_Clave.Name = "textBox_Clave";
            this.textBox_Clave.Size = new System.Drawing.Size(100, 20);
            this.textBox_Clave.TabIndex = 1;
            this.textBox_Clave.TextChanged += new System.EventHandler(this.textBox_Clave_TextChanged);
            // 
            // textBox_Nombre
            // 
            this.textBox_Nombre.Location = new System.Drawing.Point(145, 88);
            this.textBox_Nombre.Name = "textBox_Nombre";
            this.textBox_Nombre.Size = new System.Drawing.Size(100, 20);
            this.textBox_Nombre.TabIndex = 2;
            this.textBox_Nombre.TextChanged += new System.EventHandler(this.textBox_Nombre_TextChanged);
            // 
            // textBox_Dia
            // 
            this.textBox_Dia.Location = new System.Drawing.Point(262, 88);
            this.textBox_Dia.Name = "textBox_Dia";
            this.textBox_Dia.Size = new System.Drawing.Size(100, 20);
            this.textBox_Dia.TabIndex = 3;
            this.textBox_Dia.TextChanged += new System.EventHandler(this.textBox_Dia_TextChanged);
            // 
            // textBox_Cantidad
            // 
            this.textBox_Cantidad.Location = new System.Drawing.Point(379, 88);
            this.textBox_Cantidad.Name = "textBox_Cantidad";
            this.textBox_Cantidad.Size = new System.Drawing.Size(100, 20);
            this.textBox_Cantidad.TabIndex = 4;
            this.textBox_Cantidad.TextChanged += new System.EventHandler(this.textBox_Cantidad_TextChanged);
            // 
            // label_Veces
            // 
            this.label_Veces.AutoSize = true;
            this.label_Veces.Location = new System.Drawing.Point(28, 16);
            this.label_Veces.Name = "label_Veces";
            this.label_Veces.Size = new System.Drawing.Size(10, 13);
            this.label_Veces.TabIndex = 5;
            this.label_Veces.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Clave";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(145, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Nombre Artículo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(262, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Día de Venta";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(379, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Cantidad";
            // 
            // button_Next
            // 
            this.button_Next.Location = new System.Drawing.Point(145, 129);
            this.button_Next.Name = "button_Next";
            this.button_Next.Size = new System.Drawing.Size(217, 45);
            this.button_Next.TabIndex = 10;
            this.button_Next.Text = "Almacenar y Continuar";
            this.button_Next.UseVisualStyleBackColor = true;
            this.button_Next.Click += new System.EventHandler(this.button_Next_Click);
            // 
            // button_StoreTxt
            // 
            this.button_StoreTxt.Enabled = false;
            this.button_StoreTxt.Location = new System.Drawing.Point(145, 180);
            this.button_StoreTxt.Name = "button_StoreTxt";
            this.button_StoreTxt.Size = new System.Drawing.Size(216, 23);
            this.button_StoreTxt.TabIndex = 11;
            this.button_StoreTxt.Text = "Finalizar";
            this.button_StoreTxt.UseVisualStyleBackColor = true;
            this.button_StoreTxt.Click += new System.EventHandler(this.button_StoreTxt_Click);
            // 
            // button_ShowAll
            // 
            this.button_ShowAll.Enabled = false;
            this.button_ShowAll.Location = new System.Drawing.Point(382, 129);
            this.button_ShowAll.Name = "button_ShowAll";
            this.button_ShowAll.Size = new System.Drawing.Size(97, 45);
            this.button_ShowAll.TabIndex = 12;
            this.button_ShowAll.Text = "Mostrar Todos (Aquí)";
            this.button_ShowAll.UseVisualStyleBackColor = true;
            this.button_ShowAll.Click += new System.EventHandler(this.button_ShowAll_Click);
            // 
            // button_Exit
            // 
            this.button_Exit.Location = new System.Drawing.Point(402, 12);
            this.button_Exit.Name = "button_Exit";
            this.button_Exit.Size = new System.Drawing.Size(98, 29);
            this.button_Exit.TabIndex = 13;
            this.button_Exit.Text = "Salir";
            this.button_Exit.UseVisualStyleBackColor = true;
            this.button_Exit.Click += new System.EventHandler(this.button_Exit_Click);
            // 
            // button_Calcular
            // 
            this.button_Calcular.Enabled = false;
            this.button_Calcular.Location = new System.Drawing.Point(379, 180);
            this.button_Calcular.Name = "button_Calcular";
            this.button_Calcular.Size = new System.Drawing.Size(100, 23);
            this.button_Calcular.TabIndex = 14;
            this.button_Calcular.Text = "Calcular";
            this.button_Calcular.UseVisualStyleBackColor = true;
            this.button_Calcular.Click += new System.EventHandler(this.button_Calcular_Click);
            // 
            // button_GoDos
            // 
            this.button_GoDos.Enabled = false;
            this.button_GoDos.Location = new System.Drawing.Point(382, 209);
            this.button_GoDos.Name = "button_GoDos";
            this.button_GoDos.Size = new System.Drawing.Size(100, 23);
            this.button_GoDos.TabIndex = 15;
            this.button_GoDos.Text = "Ir a DOS";
            this.button_GoDos.UseVisualStyleBackColor = true;
            this.button_GoDos.Click += new System.EventHandler(this.button_GoDos_Click);
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(298, 13);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(98, 29);
            this.buttonReset.TabIndex = 16;
            this.buttonReset.Text = "Reiniciar";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // UNO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 235);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.button_GoDos);
            this.Controls.Add(this.button_Calcular);
            this.Controls.Add(this.button_Exit);
            this.Controls.Add(this.button_ShowAll);
            this.Controls.Add(this.button_StoreTxt);
            this.Controls.Add(this.button_Next);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_Veces);
            this.Controls.Add(this.textBox_Cantidad);
            this.Controls.Add(this.textBox_Dia);
            this.Controls.Add(this.textBox_Nombre);
            this.Controls.Add(this.textBox_Clave);
            this.Controls.Add(this.textBox_Veces);
            this.Name = "UNO";
            this.Text = "Desarrollo de Apps.NET - Luis Azcuaaga (Parcial 1)";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_Veces;
        private System.Windows.Forms.TextBox textBox_Clave;
        private System.Windows.Forms.TextBox textBox_Nombre;
        private System.Windows.Forms.TextBox textBox_Dia;
        private System.Windows.Forms.TextBox textBox_Cantidad;
        private System.Windows.Forms.Label label_Veces;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button_Next;
        private System.Windows.Forms.Button button_StoreTxt;
        private System.Windows.Forms.Button button_ShowAll;
        private System.Windows.Forms.Button button_Exit;
        private System.Windows.Forms.Button button_Calcular;
        private System.Windows.Forms.Button button_GoDos;
        private System.Windows.Forms.Button buttonReset;
    }
}

