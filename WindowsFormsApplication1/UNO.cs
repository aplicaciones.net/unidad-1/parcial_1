﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class UNO : Form
    {
        Boolean firstTime = true;
        Int16[] claves;
        String[] nombres;
        Int16[] dias;
        Int16[] cantidades;

        Int16 currentTime = 0;
        Int16 maxTimes = 0;

        Int16 temp_Clave;
        String temp_Nombre;
        Int16 temp_Dia;
        Int16 temp_Cant;

        float promedio = 0;
        String mostSold = "";


        WindowsFormsApplication2.DOS DOS = new WindowsFormsApplication2.DOS();
        LibreriaMagica.libreria libreria = new LibreriaMagica.libreria();

        public UNO()
        {
            InitializeComponent();
        }

        private void button_Next_Click(object sender, EventArgs e)
        {
            if (textBox_Veces.Text == "") //If no number set, then set 1.
            {
                textBox_Veces.Text = "1";
            }

            if (firstTime)
            {
                firstTime = false;
                textBox_Veces.ReadOnly = true;
                maxTimes = Convert.ToInt16(textBox_Veces.Text);
                //Initialize arrays as 'Veces' text.
                claves = new Int16[maxTimes];
                nombres = new String[maxTimes];
                dias = new Int16[maxTimes];
                cantidades = new Int16[maxTimes];
            }

            //Store all
            claves[currentTime] = temp_Clave;
            nombres[currentTime] = temp_Nombre;
            dias[currentTime] = temp_Dia;
            cantidades[currentTime] = temp_Cant;
            currentTime++;
            label_Veces.Text = Convert.ToString(currentTime);

            if (currentTime >= maxTimes)
            {
                button_Next.Enabled = false;
                button_StoreTxt.Enabled = true;

            }
            //textBox_Veces.Text = "";
            textBox_Clave.Text = "";
            textBox_Nombre.Text = "";
            //textBox_Dia.Text = "";
            textBox_Cantidad.Text = "";
        }

        private void textBox_Clave_TextChanged(object sender, EventArgs e)
        {
            if (textBox_Clave.Text != "")
            {
                temp_Clave = Convert.ToInt16(textBox_Clave.Text);
            }
        }

        private void textBox_Nombre_TextChanged(object sender, EventArgs e)
        {
            if (textBox_Nombre.Text != "")
            {
                temp_Nombre = textBox_Nombre.Text;
            }
        }

        private void textBox_Dia_TextChanged(object sender, EventArgs e)
        {
            if (textBox_Clave.Text != "")
            {
                temp_Dia = Convert.ToInt16(textBox_Dia.Text);
            }
        }

        private void textBox_Cantidad_TextChanged(object sender, EventArgs e)
        {
            if (textBox_Clave.Text != "")
            {
                temp_Cant = Convert.ToInt16(textBox_Cantidad.Text);
            }
        }

        private void button_StoreTxt_Click(object sender, EventArgs e)
        {
            button_Next.Enabled = false;
            button_ShowAll.Enabled = true;
            button_Calcular.Enabled = true;
            button_GoDos.Enabled = true;

            // Once it ends, store info on txt
            StreamWriter writer;
            writer = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/DatosUno.azk", false);
            for (Int16 i = 0; i < currentTime; i++)
            {
                writer.WriteLine(claves[i]);
                writer.WriteLine(nombres[i]);
                writer.WriteLine(dias[i]);
                writer.WriteLine(cantidades[i]);
            }
            writer.Close();
        }

        private void button_ShowAll_Click(object sender, EventArgs e)
        {
            StreamReader reader;
            reader = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/DatosUno.azk");

            label_Veces.Text = Convert.ToString(currentTime);
            if (textBox_Veces.Text == "")
            {
                textBox_Veces.Text = "1";
            }
            for (Int16 loop = 0; loop < currentTime; loop++)
            {

                textBox_Clave.Text = reader.ReadLine();
                textBox_Nombre.Text = reader.ReadLine();
                textBox_Dia.Text = reader.ReadLine();
                textBox_Cantidad.Text = reader.ReadLine();

                textBox_Clave.Refresh();
                textBox_Nombre.Refresh();
                textBox_Dia.Refresh();
                textBox_Cantidad.Refresh();

                System.Threading.Thread.Sleep(1500);
            }
            reader.Close();

        }

        private void button_Exit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button_Calcular_Click(object sender, EventArgs e)
        {

            if (cantidades.Length > 0)
            {
                promedio = libreria.promedios(cantidades);
                mostSold = nombres[libreria.mostSold(cantidades)];
            }
        }

        private void button_GoDos_Click(object sender, EventArgs e)
        {
            WindowsFormsApplication2.DOS DOS = new WindowsFormsApplication2.DOS();
            DOS.promedio = promedio;
            DOS.masVendido = mostSold;
            DOS.Show();
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            firstTime = true;
            textBox_Veces.ReadOnly = false;
            currentTime = 0;
            label_Veces.Text = "";
            maxTimes = 0;
            textBox_Veces.Text = "";
            temp_Clave = 0;
            temp_Nombre = "";
            temp_Dia = 0;
            temp_Cant = 0;

            textBox_Clave.Text = "";
            textBox_Nombre.Text = "";
            textBox_Dia.Text = "";
            textBox_Cantidad.Text = "";


            promedio = 0;
            mostSold = "";

            button_Next.Enabled = true;
            button_ShowAll.Enabled = false;
            button_Calcular.Enabled = false;
            button_GoDos.Enabled = false;
            button_StoreTxt.Enabled = false;

        }
    }
}
