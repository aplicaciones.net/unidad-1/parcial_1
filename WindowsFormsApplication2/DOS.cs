﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class DOS : Form
    {
        public float promedio = 0;
        public String masVendido = "n/a";



        public DOS()
        {
            InitializeComponent();
        }

        private void button_Back_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button_ShowResults_Click(object sender, EventArgs e)
        {
            label_Average.Text = Convert.ToString(promedio);
            label_MostSold.Text = masVendido;
        }
    }
}
