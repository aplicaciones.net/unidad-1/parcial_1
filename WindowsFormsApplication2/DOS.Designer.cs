﻿namespace WindowsFormsApplication2
{
    partial class DOS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_Average = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label_MostSold = new System.Windows.Forms.Label();
            this.button_ShowResults = new System.Windows.Forms.Button();
            this.button_Back = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_Average
            // 
            this.label_Average.AutoSize = true;
            this.label_Average.Location = new System.Drawing.Point(178, 66);
            this.label_Average.Name = "label_Average";
            this.label_Average.Size = new System.Drawing.Size(10, 13);
            this.label_Average.TabIndex = 0;
            this.label_Average.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Promedio Ventas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Lo más vendido fue";
            // 
            // label_MostSold
            // 
            this.label_MostSold.AutoSize = true;
            this.label_MostSold.Location = new System.Drawing.Point(178, 109);
            this.label_MostSold.Name = "label_MostSold";
            this.label_MostSold.Size = new System.Drawing.Size(10, 13);
            this.label_MostSold.TabIndex = 3;
            this.label_MostSold.Text = "-";
            // 
            // button_ShowResults
            // 
            this.button_ShowResults.Location = new System.Drawing.Point(46, 169);
            this.button_ShowResults.Name = "button_ShowResults";
            this.button_ShowResults.Size = new System.Drawing.Size(75, 23);
            this.button_ShowResults.TabIndex = 4;
            this.button_ShowResults.Text = "Mostrar";
            this.button_ShowResults.UseVisualStyleBackColor = true;
            this.button_ShowResults.Click += new System.EventHandler(this.button_ShowResults_Click);
            // 
            // button_Back
            // 
            this.button_Back.Location = new System.Drawing.Point(181, 169);
            this.button_Back.Name = "button_Back";
            this.button_Back.Size = new System.Drawing.Size(75, 23);
            this.button_Back.TabIndex = 5;
            this.button_Back.Text = "Volver";
            this.button_Back.UseVisualStyleBackColor = true;
            this.button_Back.Click += new System.EventHandler(this.button_Back_Click);
            // 
            // DOS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.button_Back);
            this.Controls.Add(this.button_ShowResults);
            this.Controls.Add(this.label_MostSold);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_Average);
            this.Name = "DOS";
            this.Text = "DOS";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Average;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_MostSold;
        private System.Windows.Forms.Button button_ShowResults;
        private System.Windows.Forms.Button button_Back;
    }
}

